"""
Converts DaVinci Resolve metadata csv files into importable xmeml files.
This effectively allows to import and export clip and sub-clips between projects.
This tool has been tested with DaVinci Resolve 16.1.0.B25, Python 2.7.15 and 
Python 3.7.2 (without --rename option)

Basic Usage:

To export a csv file, you can use the "File > Export Metadata From" menu in Resolve.
To import back the csv file, use this tool:
  1. Converts the csv to a xml file using the command "resolve_csvToXml metadata.csv"
  2. Drag and drop metadata.csv.xml into your project bin
  3. Use resolve built-in "File > Import Metadata To" menu to bring back all
     metadatas onto imported clips

Limitations:

  1. Clip names (From CSV "EDL Clip Name" column) are lost when importing using
     the xml file. This tool provides a way to rename them using the resolve API
     (Resolve Studio only, you'll need it in your python path). To enable it,
     add the "-r/--rename" argument to your command, i.e :
     "resolve_csvToXml matadata.csv -r". This will pause the program after the
     export (allowing you to drag and drop the xml file), and rename imported
     clips within resolve once you hit enter. Note renaming is WAY faster if
     you're not currently displaying the bin containing the imported clips.
  2. By default, this tool create subclips only if they don't start at frame 0.
     However, you can force all clips to be subclips using the --s/--subclip option.
  3. Currenly, using "-m/--metadatas" to export metadatas to the XML file does not
     play nicely with subclips (Clips and their subclips would all take the same
     metadatas), therefore it is preferable to only use the tool to import clip
     and subclips, then re-import the metadata.csv file using the built-in import
     tool in Resolve.

Additionnal Notes:

This tool is highly "tweaky" and I advise you to double-check imported clips
whenever possible. (I hope Black Magic Design will add a build-in proper import
function some day).
If you happen to know better ways or would like to share thoughts, feel free
to contact me at framestamp@gmail.com.
"""

__author__ = 'Gilles de Saint-Aubert'
__email__ = 'framestamp@gmail.com'
__copyright__ = 'framestamp.io'

import os
import re
import io
import csv
import sys
import argparse

# XMEML Simple structure template

XMEML = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xmeml>
<xmeml version="5">
{XMEML_CLIPS}
</xmeml>
"""

XMEML_CLIP = """
  <clip>
    <duration>{FRAMES}</duration>
    <rate><timebase>{FRAME_RATE}</timebase><ntsc>TRUE</ntsc></rate>
    <name>{EDL_CLIP_NAME}</name>
    <media>
      <video>
        <track>
          <clipitem>
            <name>{EDL_CLIP_NAME}</name>
            <duration>{FRAMES}</duration>
            <rate><timebase>{FRAME_RATE}</timebase><ntsc>TRUE</ntsc></rate>
            <file><pathurl>file://localhost/{FILE_PATH}</pathurl></file>
            {XMEML_SUBCLIP}     
          </clipitem>
        </track>
      </video>
    </media>
  {XMEML_METADATAS}
  {XMEML_TAGS}
  {XMEML_MARKERS}
  </clip>
"""

XMEML_METADATAS = """
    <logginginfo>
      <description>{DESCRIPTION}</description>
      <scene>{SCENE}</scene>
      <shottake>{SHOT}</shottake>
      <good>{GOOD_TAKE}</good>
    </logginginfo>
"""

XMEML_TAGS = """
    <labels>
        <label>{KEYWORDS}</label>
    </labels>
"""

_XMEML_MARKERS = """
    <marker>
      <in>50</in>
      <out>55</out>
      <comment>Comment for marker</comment>
      <name>My Marker</name>
      <color>Red (SKIPPED)</color>
    </marker>
"""

XMEML_SUBCLIP = """
            <subclipinfo>
              <startoffset>{START_FRAME}</startoffset>
              <endoffset></endoffset>
            </subclipinfo>'
"""

def clipXml(props, force_subclip=False, include_meta=False):
    """ Returns XML code for given clip dict.
        
    Args:
        props: Property dict (column name: row value)
        force_subclip: Force all clips to be subclips. By default, only clips
            not starting at 0 will be considered as subclip.
        include_meta: Bool, True will also export logging and keywords

    Returns:
        string, xml code
    """

    # Export keyword as tags
    if include_meta and 'Keywords' in props:
        tags = XMEML_TAGS.format(KEYWORDS=props['Keywords'])
    else:
        tags = ''

    # Export some metadatas as logging infos
    if include_meta:
        metadatas = XMEML_METADATAS.format(
            DESCRIPTION = props.get('Description', ''),
            SCENE       = props.get('Scene', ''),
            SHOT        = props.get('Shot', ''),
            GOOD_TAKE   = props.get('Good Take', '0'),
            )
    else:
        metadatas = ''

    # Set subclip infos
    if force_subclip or props['Start Frame'] != '0':
        subclip = XMEML_SUBCLIP.format(START_FRAME=props['Start Frame'])
    else:
        subclip = ''

    # Set clip path
    clip_path = props['Clip Directory']+'/'+props['File Name']
    clip_path = clip_path.replace('\\', '/')

    # Return all together
    # Note framerate has to be rounded in order to have correct imported ranges
    return re.sub('\n( *\n)*', '\n', XMEML_CLIP.format(
        FRAMES          = props['Frames'],
        FRAME_RATE      = round(float(props['Frame Rate'])),
        EDL_CLIP_NAME   = props['EDL Clip Name'],
        FILE_PATH       = clip_path,
        XMEML_METADATAS = metadatas,
        XMEML_TAGS      = tags,
        XMEML_SUBCLIP   = subclip,
        XMEML_MARKERS   = '',
        ))

def listAllClips(api_folder):
    """ Returns list of all clips within the given resolve folder """
    clips = api_folder.GetClips().values()
    for subfolder in api_folder.GetSubFolders().values():
        clips.extend(listAllClips(subfolder))
    return clips

class Error(Exception):
    pass

def csvToXml(csv_path, xml_path=None, name='*', keyword='*', limit=None,
                tags=tuple(), rename=False, verbose=False, include_meta=False,
                force_subclip=False):
    """ Export and return xmeml file from resolve's metadata.csv file.

    Args:
        csv_path: Full path to csv
        xml_path: If provided, full path to xml file to write (will be overwritten).
            Specifying '-' will print out xml.
        name: Pattern to filter names (wildcards accepted)
        keyword: Pattern to filter clips by keyword (wildcards accepted)
        limit: If provided, limit xml output to this many clips.
        tags: tuple or list, tag to add on each exported clips (include_meta
            must be true)
        rename: Bool, True will pause and wait for user input to attempt to
            rename imported clips using the resolve api.
        verbose: Bool, True to print more stuff (filtered-out items mostly).
        include_meta: Bool, True to export some basic metadatas in the xml file.
        force_subclip: Bool, True to always make subclips.

    Returns:
        String, complete xmeml-ich code.
    """

    # Verify csv exists
    if not os.path.exists(csv_path):
        raise Error('Unable to locate {}'.format(path))

    # Read csv, produce a dict for each row
    with io.open(csv_path, 'r', encoding='utf-16') as f:
        reader = csv.DictReader(f)
        all_clips = list(reader)

    # Limit output
    if limit is not None:
        all_clips = all_clips[:limit]

    # Verify we have read something & have the required columns
    if len(all_clips) == 0:
        raise Error('Csv file contains no clips')
    for key in ('Frame Rate', 'Start Frame', 'Frames', 'EDL Clip Name', 'File Name', 'Clip Directory'):
        if key not in all_clips[0]:
            raise Error('Missing column name "{}"'.format(key))

    # Default EDL Clip Name to File Name, add tags
    for clip in all_clips:
        if clip['EDL Clip Name'] == '':
            clip['EDL Clip Name'] = clip['File Name']
        if len(tags) > 0:
            ntags = clip.get('Keywords', '').split(',') + tags
            clip['Keywords'] = ','.join(ntags)


    # Filter list
    re_filter  = re.compile('^'+name.replace('*', '.*?')+'$', re.I)
    re_keyword = re.compile('(^|,)'+keyword.replace('*', '.*?')+'(,|$)', re.I)
    clips  = []
    for clip in all_clips:
        if clip['Clip Directory'] == '':
            print('Skipped: {} (Not a clip)'.format(clip['EDL Clip Name']))
            continue
        if re_filter.match(clip['EDL Clip Name']) is None:
            if verbose:
                print('Skipped: {} (name filter)'.format(clip['EDL Clip Name']))
            continue
        if keyword != '*' and re_keyword.match(clip.get('Keywords', '')) is None:
            if verbose:
                print('Skipped: {} (keyword filter)'.format(clip['EDL Clip Name']))
            continue
        clips.append(clip)

    # Verify we have some clips lefs
    if len(clips) == 0:
        raise Error('No clip to export')

    # Generate xml
    xmls = [
        clipXml(clip, force_subclip=force_subclip, include_meta=include_meta)
        for clip in clips
        ]
    xml = XMEML.format(
        XMEML_CLIPS = '\n'.join(xmls)
        )[1:]

    # Write xml
    if xml_path == '-':
        print(xml)
    elif xml_path is not None:
        with open(xml_path, 'w') as f:
            f.write(xml)

    print('DONE: {} clip(s) exported.'.format(len(clips)))

    if rename:

        # Pause script
        msg = 'Drag-n-drop xml file in resolve, then hit enter to update clip names from EDL CLip Name field'
        try:
            raw_input(msg)
        except:
            input(msg)

        # Get list of all clips in current project
        app       = dvr_script.scriptapp('Resolve')
        manager   = app.GetProjectManager()
        project   = manager.GetCurrentProject()
        pool      = project.GetMediaPool()
        api_clips = listAllClips(pool.GetRootFolder())

        # Key by File Name, Start and End frame
        search = {
            (c['File Name'], c['Start Frame'], c['End Frame']) : c['EDL Clip Name'] for c in clips
        }

        # Search for clips to rename
        for clip in api_clips:
            props = clip.GetClipProperty()
            h = (props['File Name'], props['Start'], props['End'])
            name = search.pop(h, None)
            if name is not None:
                if name == props['Clip Name']:
                    if verbose:
                        print('Skipped : {} (Already renamed)'.format(name))
                else:
                    print('Renamed {} to {}'.format(props['Clip Name'], name))
                    clip.SetClipProperty('Clip Name', name)

        for name in search.values():
            print("Not Found : "+name)

        print('DONE.')
    return xml


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('csv_path', type=str, help='Path to CSV file to convert')
    parser.add_argument('xml_path', type=str, nargs='?', help='Path to XML file to write. Defaults to {csv}.xml. Use "-" to print to stdout')
    parser.add_argument('-r', '--rename',    action='store_true', help='Once exported, wait for user import to rename imported clips within resolve (Studio API needed)')
    parser.add_argument('-v', '--verbose',   action='store_true', help='Print more stuff')
    parser.add_argument('-s', '--subclip',   action='store_true', help='Force all clips to be subclips (even thoses starting at frame 0)')
    parser.add_argument('-m', '--metadatas', action='store_true', help='Also include Shot,Take,Description and Keywords in exported xml')
    parser.add_argument('-t', '--tags',     type=str, metavar='', help='Comma-separated list of tags (keywords) to add to each clips. To use with -m/--metadatas')
    parser.add_argument('-n', '--limit',    type=int, metavar='', help='Limit output to the n-th first clips')
    parser.add_argument('-f', '--filter',   type=str, metavar='', help='Only export clips matching the given clip name pattern', default='*')
    parser.add_argument('-k', '--keyword',  type=str, metavar='', help='Only export clips matching the given keyword pattern', default='*')

    args = parser.parse_args()

    # Attempt to import resolve api
    if args.rename:
        if sys.version_info.major == 3:
            print('INFO: Python 3.X not tested with resolve API, may crash there.')
        try:
            import DaVinciResolveScript as dvr_script
        except ImportError:
            api_dir = 'C:/ProgramData/Blackmagic Design/DaVinci Resolve/Support/Developer/Scripting/Modules'
            if os.path.isdir(api_dir):
                import sys
                sys.path.append(api_dir)
                try:
                    import DaVinciResolveScript as dvr_script
                    print('INFO: DaVinciResolveScript was dynamically added to PYTHONPATH')
                except ImportError:
                    print('ERROR: Unable to import DaVinciResolveScript')
                    exit(1)
            else:
                print('ERROR: Unable to find DaVinciResolveScript module')
                exit(1)

    # Set csv path
    csv_path = os.path.abspath(args.csv_path)

    # Set xml path
    if args.xml_path is None:
        xml_path = csv_path+'.xml'
    elif args.xml_path == '-':
        xml_path = '-'
    else:
        xml_path = os.path.abspath(args.xml_path)

    # Tags
    tags = [] if args.tags is None else [t.strip() for t in args.tags.split(',')]

    try:
        csvToXml(csv_path, xml_path, tags=tags, limit=args.limit, rename=args.rename,
            name=args.filter, keyword=args.keyword, verbose=args.verbose,
            include_meta=args.metadatas, force_subclip=args.subclip)
    except Error as e:
        print('\nERROR: {} (Aborted)'.format(e))

