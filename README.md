This repository contains some random python scripts for DaVinci Resolve (Well,
only one currently, that's a start !).

Resolve CSV to XML
==================

File: [resolve_csvToXml.py](scripts/resolve_csvToXml.py)

This standalone script allows to convert resolve "metadata.csv" files into
importable xml files, effectively allowing to import/export clip and subclips
between projects.

Minimal example (will output metadata.csv.xml):

```batch
python resolve_csvToXml.py path/to/metadata.csv
```

Complete description and options:

```batch
python resolve_csvToXml.py -h
```